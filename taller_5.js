const span = document.getElementById("mes");
const span1 = document.getElementById("alimento");
const botonMes = document.getElementById("btnMes");
const botonAlimento = document.getElementById("btnAlimento");
const clearMes = document.getElementById("clearM");
const clearAlimento = document.getElementById("clearA");

let meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'noviembre', 'diciembre']

let manzana = new Producto_alimenticio(001, "manzana", 0.5)
let fideo = new Producto_alimenticio(002, "fideo", 2.1)
let sal = new Producto_alimenticio(003, "sal", 1.5)

let alimentos = [manzana, fideo, sal]

clearMes.addEventListener('click', limpiar);
clearAlimento.addEventListener('click', limpiar1);
botonMes.addEventListener('click', mostrarMeses);
botonAlimento.addEventListener("click", mostrarAlimentos);

function Producto_alimenticio(codigo, nombre, precio) {
    this.codigo = codigo
    this.nombre = nombre
    this.precio = precio
    this.imprimeDatos = () => `codigo: ${this.codigo}       nombre: ${this.nombre}      precio:${this.precio}`
}

function mostrarAlimentos(){
    for(let alimento of alimentos){
        let p = document.createElement("P") 
        let text = document.createTextNode(alimento.imprimeDatos()) 
        p.appendChild(text);
        span1.appendChild(p);
    }
}

function mostrarMeses(){
    for(mes of meses){
        let p = document.createElement("P") 
        let text = document.createTextNode(`${mes}`) 
        p.appendChild(text);
        span.appendChild(p);
        console.log(mes)
    }
}

function limpiar(){
    span.innerHTML='';
}
function limpiar1(){
    span1.innerHTML='';
}